export const raw = (contents) => ({ type: "tile", contents });
// empty contents means the map is empty
export const fresh = () => raw([]);