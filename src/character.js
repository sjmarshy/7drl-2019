import { entity } from "./entity";

function position(x, y) {
  return { type: "position", x, y };
}

function controllable() {
  return { type: "controllable" };
}

export function playerEntity(x = 0, y = 0) {
  return entity(position(x, y), controllable());
}
