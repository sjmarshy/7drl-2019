import { v4 as uuid } from "uuid";

function rawEntity(id, ...components) {
  return {
    id,
    components
  };
}

export function entity(...components) {
  return rawEntity(uuid(), ...components);
}

export const componentOfType = type => c => c.type === type;
export const entityHasComponentOfType = type => entity =>
  entity.components.filter(componentOfType(type)).length > 0;
