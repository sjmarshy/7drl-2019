import { fresh as freshMap } from "./map";
import { entity, entityHasComponentOfType, componentOfType } from "./entity";
import { playerEntity } from "./character";

const entities = [entity(freshMap()), playerEntity(4, 4)];

// a canvas rendering context
function renderMap(es, ctx) {
  ctx.save();

  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  const maps = es.filter(entityHasComponentOfType("map"));
  maps.forEach(mapEntity => {
    mapEntity.components.filter(componentOfType("map")).map(({ tiles }) => {
      tiles.forEach((row, y) => {
        row.forEach((tile, x) => {
          if (tile.contents.length === 0) {
            // draw an X
            // 16x16 tiles on a 16x16 grid
            ctx.fillStyle = "white";
            ctx.font = "30pt monospace";
            ctx.fillText("■", x * 32 + 8, y * 32 + 32);
          }
        });
      });
    });
  });

  ctx.restore();
}

function renderPlayer(es) {
  const player = es.filter(
    and(
      entityHasComponentOfType("position"),
      entityHasComponentOfType("controllable")
    )
  );
}

const context = document.getElementById("game").getContext("2d");

renderMap(entities, context);
