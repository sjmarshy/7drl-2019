import { fresh as freshTile } from './tile';
// tiles is a 16*16 array, where each sub-array 
//   is a row and each element is a tile
export const raw = (tiles) => ({ type: 'map', tiles });

export const fresh = () => raw(new Array(16).fill(new Array(16).fill(freshTile())));